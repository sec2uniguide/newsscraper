package com.lydia.news.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.lydia.news.domains.News;

@Controller
public class HomeController {
	
	@GetMapping("/")
	public String index(Model model) {
		
		List<String> capArgs = new ArrayList<String>();
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setJavascriptEnabled(true);                
		caps.setCapability("takesScreenshot", true);  
		caps.setCapability(
		                        PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
		                        "C:\\phantomjs\\phantomjs.exe"
		                    );
		caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, capArgs);
		capArgs.add("--load-images=false");
		
		WebDriver driver = new  PhantomJSDriver(caps);		
		
		List<News> newsList = new ArrayList<News>();
	    
		String reporterUrlMain = "https://www.thereporterethiopia.com/";
	    String reporterUrlNews = "https://www.thereporterethiopia.com/news";
	    
	    String addisUrl = "https://addisstandard.com/";

	    
	    driver.get(reporterUrlNews);
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    List<WebElement> reporternewsDivs = driver.findElements(By.cssSelector(".post-block.display-term.margin-bottom-30"));
	    for(int i = 0; i < 5; i++) {
	    	WebElement div1 = reporternewsDivs.get(i).findElement(By.cssSelector(".post-content"));
	    	WebElement div2 = div1.findElement(By.cssSelector(".post-title"));
	    	WebElement a = div2.findElement(By.tagName("a"));
	    	WebElement div3 = div1.findElement(By.cssSelector(".post-body"));
	    	
	    	News reporterTopNews = new News();
	        reporterTopNews.setImagePath("");
	        reporterTopNews.setTitle(div2.getText());
	        reporterTopNews.setLink(a.getAttribute("href"));
	        reporterTopNews.setDescription(div3.getText());
	    	
	        newsList.add(reporterTopNews);
	    	System.out.println(".............\n" + a.getAttribute("href"));
	    }
    
	    driver.navigate().to(addisUrl);
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    ArrayList<String> links= new ArrayList<String>();
	    List<WebElement> newsElement = driver.findElements(By.className("vw-post-box-title"));
        for ( int i =0; i<5; i++) {
            String link=newsElement.get(i).findElement(By.tagName("a")).getAttribute("href");
            links.add(link);
            System.out.println(link);

        }
        for (String link: links) {
            driver.navigate().to(link);

            String title = driver.findElement(By.className("vw-page-title")).getText();
            System.out.println("Title: "+title);

            String image = driver.findElements(By.tagName("img")).get(0).getAttribute("src");
            System.out.println("Image: "+image);
            
            News addisTopNews = new News();
            addisTopNews.setImagePath("");
            addisTopNews.setTitle(title);
            addisTopNews.setLink(link);
            
            List<WebElement> detailsP = driver.findElements(By.tagName("p"));
            for (WebElement p: detailsP) {
                if(p.getCssValue("color").equals("rgba(76, 76, 76, 1)")){
                	addisTopNews.setDescription(p.getText());
                }
            }
            
            newsList.add(addisTopNews);


	    }
	    
        model.addAttribute("newsList", newsList);
        driver.close();
		return "index";
		
	}
}
