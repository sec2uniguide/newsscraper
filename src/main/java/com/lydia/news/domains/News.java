package com.lydia.news.domains;

import lombok.Data;

@Data
public class News {
	private  long id;
	
	private String title;
	
	private String description;
	
	private String link;
	
	private String imagePath;
	
	
}
